<!DOCTYPE html>
<html lang="en">
<head>
   <title>Signup</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
   <!-- google Adsense start -->
   <script data-ad-client="ca-pub-6880354863748177" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- google Adsense end -->
    <script>
  (function(){

    window.ldfdr = window.ldfdr || {};
    (function(d, s, ss, fs){
      fs = d.getElementsByTagName(s)[0];

      function ce(src){
        var cs  = d.createElement(s);
        cs.src = src;
        setTimeout(function(){fs.parentNode.insertBefore(cs,fs)}, 1);
      }

      ce(ss);
    })(document, 'script', 'https://sc.lfeeder.com/lftracker_v1_DzLR5a593bYaBoQ2.js');
  })();
</script>

    <!-- Leadfeeder1998 Tracker end 2 -->
</head>
<style>
  body{
      overflow-x:hidden;
  }
  .main-content{
      width:50%;
      height:40%;
      margin:10px auto;
      background-color: #fff;
      border:2px solid #e6e6e6;
      padding: 40px 50px;
  }
  .header{
      border: 0px solid #000;
      margin-bottom: 5px;
  }
  .well{
      background-color: #187FAB;
  }
  #signup{
      width:60%;
      border-radius: 30px;
  }

</style>
<body>
<div class="row">
        <div class="col-sm-12">
           <div class="well">
              <h2><a href="index.php" style="color: white;">Back to Home</a></h2>
              <center><h1 style="color: white;">Kagerma</h1></center>
            
           </div>
        </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="main-content">
         <div class="header">
           <h3 style="text-align:center;"><strong>Add your business to Kagerma</strong> </h3><hr>
         </div>
         <div class="l-part">
           <form action="insertBusiness.php" method="post">
              <div class="input-group">
                <span class="input-group-addon"><i class= "glyphicon glyphicon-pencil"></i></span>
                <input type="text" class="form-control" placeholder="First Name" name="first_name" required="required"></input>
              </div><br>
              <div class="input-group">
                <span class="input-group-addon"><i class= "glyphicon glyphicon-pencil"></i></span>
                <input type="text" class="form-control" placeholder="Last Name" name="last_name" required="required"></input>
              </div><br>
             
              <div class="input-group">
                <span class="input-group-addon"><i class= "glyphicon glyphicon-user"></i></span>
                <input id="email" type="email" class="form-control" placeholder="Email" name="b_email" required="required"></input>
              </div><br>
              <div class="input-group">
                <span class="input-group-addon"><i class= "glyphicon glyphicon-chevron-up"></i></span>
                <input class="form-control" name="o_country" required="required" placeholder="Country of Origin">  </input>  
              
              </div><br>
              <div class="input-group">
                <span class="input-group-addon"><i class= "glyphicon glyphicon-chevron"></i></span>
                <input class="form-control" name="telephone" required="required" placeholder="telephone"></input>                 
              </div><br>
              <div class="input-group">
                <span class="input-group-addon"><i class= "glyphicon glyphicon-chevron-down"></i></span>
                <select class="form-control input-md" name="businessCategory" required="required">
                   <option hidden>select business category</option>
                   <option value="company">company</option>
                   <option value="ecommerce">ecommerce</option>
                   <option value="onlineplatform">online platform</option>
                   <option value="youtube">youtube</option>
                   <option value="personalservice">personal service</option>
                      
                </select>
              </div><br>
              <div class="input-group">
                <span class="input-group-addon"><i class= "glyphicon glyphicon-chevron-right"></i></span>
                <input class="form-control" name="nameOfBusiness" required="required" placeholder="business name"></input>                 
              </div><br>
              <div class="input-group">
                <span class="input-group-addon"><i class= "glyphicon glyphicon-chevron"></i></span>
                <input class="form-control" name="countryOfBusiness" required="required" placeholder="country of the business"></input>                 
              </div><br>
              <div class="input-group">
                <span class="input-group-addon"><i class= "glyphicon glyphicon-chevron"></i></span>
                 <textarea id="description" name="description" rows="10" col="20" style="min-width:500px;" >describe your business here </textarea> 
                           
              </div><br>
              <div class="input-group">
                <span class="input-group-addon"><i class= "glyphicon glyphicon-chevron-right"></i></span>
                <input class="form-control" name="linkToWebsite"   placeholder="paste link to website"></input>                 
              </div><br>
              
          
                
                <center><button id="addBusiness" class="btn btn-info btn-lg" name="addBusiness">Add Business</button> </center>
                <?php
                  include("insertBusiness.php");
                ?>
                 
           </form>
         </div>
        </div>
      </div>
    </div>
    
</body>
</html>