<?php
   require_once('connection.php');
   $searchValue = "";

   if(isset($_GET['key'])){
    $searchValue = $_GET['key'];
   }

   if(isset($_GET['page']))
   {
       $page = $_GET['page'];
   }
   else{
       $page = 1;
   }

   $num_per_page = 05;
   $start_from = ($page-1)*05;
  
     

   $query = "SELECT * from company limit $start_from, $num_per_page";
   $result = mysqli_query($con, $query);

 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Other business-Kagerma</title>
	<link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="index.js">
	<link rel="stylesheet" href="css/fixed.css">
    <link rel="stylesheet" href="css/style.css">
     <!-- google Adsense start -->
     <script data-ad-client="ca-pub-6880354863748177" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- google Adsense end -->
</head>

<body data-spy="scroll" data-target="#vabarResponsive" class="body">
<!-- Navbar start -->
<?php
 include('header.php');
 require_once('connection.php');
 $sql = "SELECT * from company order by id";
 $result = mysqli_query($con, $sql);
?>
<!-- Navbar End -->

 <div>
   <h3>
      Info about other businesses owned by Africans online.
   </h3>
 
  <!-- searching area start -->
  <?php 
   include('search.php'); 
//    require_once('connection.php');
//    $sql = "SELECT * from company order by id ";
//    $result = mysqli_query($con, $sql);
  ?>
  <!-- searching area end -->
 </div>

 <div> 
    <br> <?php include ('inCountry.php'); ?>
        <table class="table table-striped" id="myTable">
            <thead>
                <tr>
                <th scope="col">Number</th>
                <th scope="col">Company Name</th>
                <th scope="col">Description</th>
                <th scope="col">Contact/Website</th>
                <th scope="col">Country</th>
                </tr>
            </thead>
            <?php
              $number = 1;
              while($row = mysqli_fetch_assoc($result)):
            ?>
        <tbody>
                <tr>
                    <td scope="row"><?php echo $number; $number++; ?></td>
                    <td><?= $row['companyName'] ?></td>
                    <td><?= $row['description'] ?></td>
                    <td><a href=<?= $row['website'] ?>><?= $row['companyName'] ?></a></td>
                    <td><?= $row['country'] ?></td>
                </tr>
                <?php endwhile; ?>
               
                
                
        </tbody>
        </table>
        <?php
         $pr_query = "SELECT * from company";
         $pr_result = mysqli_query($con, $pr_query);
         $total_record = mysqli_num_rows($pr_result);
          
         $total_page = ceil($total_record/ $num_per_page);

         if($page>1){
            echo "<a href='otherBusiness.php?page=".($page-1)."' class='btn btn-danger'>Previous</a>";
 
         }
         
          for($i=1; $i<$total_page;$i++)
            {
             echo "<a href='otherBusiness.php?page=".$i."' class='btn btn-primary'>$i</a>";
            }

            if($i>$page){
                echo "<a href='personalService.php?page=".($page+1)."' class='btn btn-danger'>Next</a>";
     
             }
     ?>
</div>

 

 

<!-- Start Footer Sections -->
 

<!-- End Footer Section -->


  



 
<br><br><br><br><br><br><br><br><br><br>

<!--- Script Source Files -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/releases/v5.5.0/js/all.js"></script>
<script src="index.js"></script>
<script>
  $(document).ready(function() {
    filterResultTable()
  });
</script>

  <script>
    (function(){

      window.ldfdr = window.ldfdr || {};
      (function(d, s, ss, fs){
        fs = d.getElementsByTagName(s)[0];

        function ce(src){
          var cs  = d.createElement(s);
          cs.src = src;
          setTimeout(function(){fs.parentNode.insertBefore(cs,fs)}, 1);
        }

        ce(ss);
      })(document, 'script', 'https://sc.lfeeder.com/lftracker_v1_DzLR5a593bYaBoQ2.js');
    })();
  </script>

    <!-- Leadfeeder1998 Tracker end 2 -->
<!--- End of Script Source Files -->

<?php include('footer.php'); ?>
</body>

</html>

