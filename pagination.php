 <?php
   require_once('config.php');

   if($_GET['page'])
   {
       $page = $_GET['page'];
   }
   else{
       $page = 1;
   }

   $num_per_page = 05;
   $start_from = ($page-1)*05;
  
     

   $query = "SELECT * from personalservice limit $start_from, $num_per_page";
   $result = mysqli_query($con, $query);

 ?>
 
 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.css">
        <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="index.js">
        <link rel="stylesheet" href="css/fixed.css">
        <link rel="stylesheet" href="css/style.css">
     <title>Pagination in PHP with Next and Previous</title>
 </head>
 <body>
      
     <table class="table table-striped">
         <tr>
             <td>Service name</td>
             <td>Description</td>
             <td>Country</td>
         </tr>
         <tr>
             <?php 
                while($row=mysqli_fetch_assoc($result))
                {
 
             ?> 
                 <td><?php echo $row['serviceName'] ?></td>
                 <td><?php echo $row['description'] ?></td>
                 <td><?php echo $row['country'] ?></td>
         </tr>
      <?php 
          }
          ?>
     </table>
     <?php
         $pr_query = "SELECT * from personalservice ";
         $pr_result = mysqli_query($con, $pr_query);
         $total_record = mysqli_num_rows($pr_result);
          
         $total_page = ceil($total_record/ $num_per_page);

         if($page>1){
            echo "<a href='pagination.php?page=".($page-1)."' class='btn btn-danger'>Previous</a>";
 
         }
         
          for($i=1; $i<$total_page;$i++)
            {
             echo "<a href='pagination.php?page=".$i."' class='btn btn-primary'>$i</a>";
            }

            if($i>$page){
                echo "<a href='pagination.php?page=".($page+1)."' class='btn btn-danger'>Next</a>";
     
             }
     ?>

     
 </body>
 </html>