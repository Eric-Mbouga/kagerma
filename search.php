<div id="search">

<br><br><form class="form-inline mr-auto">
  <input value="<?=$searchValue?>" class="form-control mr-sm-2" id="search-field" type="search" placeholder="search by country" aria-label="Search"  onkeyup="filterResultTable()">
  <!--button class="btn blue-gradient btn-rounded btn-sm my-0" type="submit">Search</button-->
</form><br>
</div>

<script>
  function filterResultTable() {
    let input, filter, table, tr, td, i;
        input = document.getElementById("search-field");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[3];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
  }
</script>
