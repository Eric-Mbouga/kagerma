<?php
  include("connection.php");

  if(isset($_POST['addBusiness'])){

    $first_name = htmlentities(mysqli_real_escape_string($con, $_POST['first_name']));
    $last_name = htmlentities(mysqli_real_escape_string($con, $_POST['last_name']));
    $email = htmlentities(mysqli_real_escape_string($con, $_POST['b_email']));
    $countryOfOrigin = htmlentities(mysqli_real_escape_string($con, $_POST['o_country']));
    $telephone = htmlentities(mysqli_real_escape_string($con, $_POST['telephone']));
    $businessCategory = htmlentities(mysqli_real_escape_string($con, $_POST['businessCategory']));
    $nameOfBusiness = htmlentities(mysqli_real_escape_string($con, $_POST['nameOfBusiness']));
    $countryOfBusiness = htmlentities(mysqli_real_escape_string($con, $_POST['countryOfBusiness']));
    $description = htmlentities(mysqli_real_escape_string($con, $_POST['description']));
    $linkToWebsite = htmlentities(mysqli_real_escape_string($con, $_POST['linkToWebsite']));
     
   

   


        /* Inserting user start */
           $insert = $con->prepare("INSERT into users (first_name, last_name, email, countryOfOrigin, telephone) values(?, ?, ?, ?, ?)");

           $insert->bind_param("sssss", $first_name, $last_name, $email, $countryOfOrigin, $telephone);
           $insert->execute();


           # $query = mysqli_query($con, $insert);

             
             switch ($_POST['businessCategory']) {
                 case 'company':
                      $insertIntoCompanyTable = $con->prepare("INSERT into company (companyName, description, website, country)
                       values(?,?,?,?)
                      ");
                      $insertIntoCompanyTable->bind_param("ssss", $nameOfBusiness, $description,$linkToWebsite, $countryOfBusiness);
                      $insertIntoCompanyTable->execute();
                     break;
                 case 'ecommerce':
                    $insertIntoEcommerceTable = $con->prepare("INSERT into ecommerce (companyName, description, website, country)
                     values(?,?,?,?)
                    ");
                      $insertIntoEcommerceTable->bind_param("ssss", $nameOfBusiness, $description,$linkToWebsite, $countryOfBusiness);
                      $insertIntoEcommerceTable->execute();
                    break;
                 case 'onlineplatform':
                    $insertIntoOnlinePlatformTable = $con->prepare("INSERT into onlineplatforms (platformName, description, website, country)
                      values(?,?,?,?)
                    "); 
                      $insertIntoOnlinePlatformTable->bind_param("ssss", $nameOfBusiness, $description,$linkToWebsite, $countryOfBusiness);
                      $insertIntoOnlinePlatformTable->execute();
                    break; 
                 case 'youtube':
                    $insertIntoYoutubeTable = $con->prepare("INSERT into youtube (channelName, description, website, country)
                     values(?,?,?,?)
                    ");
                      $insertIntoYoutubeTable->bind_param("ssss", $nameOfBusiness, $description,$linkToWebsite, $countryOfBusiness);
                      $insertIntoYoutubeTable->execute();
                    break; 
                 case 'personalservice':
                    $insertIntoPersonalServiceTable = $con->prepare("INSERT into personalservice (serviceName, description, website, country)
                     values(?,?,?,?)
                    ");
                      $insertIntoPersonalServiceTable->bind_param("ssss", $nameOfBusiness, $description,$linkToWebsite, $countryOfBusiness);
                      $insertIntoPersonalServiceTable->execute();
                    break;           
                 
                 default:
                      echo "we need inputs from you";
                     break;
             }
        /*Inserting user end */

        header("location: index.php");
  }

?>